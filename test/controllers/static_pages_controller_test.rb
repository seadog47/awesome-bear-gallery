require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Awesome Bear Gallery"
  end

  test "should get show" do
    get :show
    assert_response :success
    assert_select "title", "Показать медведя | Awesome Bear Gallery"
  end

  test "should get upload" do
    get :upload
    assert_response :success
    assert_select "title", "Загрузить медведя | Awesome Bear Gallery"
  end

end
