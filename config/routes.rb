Rails.application.routes.draw do
  

  get 'signup'  => 'users#new'
  get 'show' => 'static_pages#show'
  get 'upload' => 'static_pages#upload'

  root 'static_pages#home'

end
